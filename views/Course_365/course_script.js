var gCoursesDB = {
	description: "This DB includes all courses in system",
	courses: [
		{
			id: 1,
			courseCode: "FE_WEB_ANGULAR_101",
			courseName: "How to easily create a website with Angular",
			price: 750,
			discountPrice: 600,
			duration: "3h 56m",
			level: "Beginner",
			coverImage: "images/courses/course-angular.jpg",
			teacherName: "Morris Mccoy",
			teacherPhoto: "images/teacher/morris_mccoy.jpg",
			isPopular: false,
			isTrending: true
		},
		{
			id: 2,
			courseCode: "BE_WEB_PYTHON_301",
			courseName: "The Python Course: build web application",
			price: 1050,
			discountPrice: 900,
			duration: "4h 30m",
			level: "Advanced",
			coverImage: "images/courses/course-python.jpg",
			teacherName: "Claire Robertson",
			teacherPhoto: "images/teacher/claire_robertson.jpg",
			isPopular: false,
			isTrending: true
		},
		{
			id: 5,
			courseCode: "FE_WEB_GRAPHQL_104",
			courseName: "GraphQL: introduction to graphQL for beginners",
			price: 850,
			discountPrice: 650,
			duration: "2h 15m",
			level: "Intermediate",
			coverImage: "images/courses/course-graphql.jpg",
			teacherName: "Ted Hawkins",
			teacherPhoto: "images/teacher/ted_hawkins.jpg",
			isPopular: true,
			isTrending: false
		},
		{
			id: 6,
			courseCode: "FE_WEB_JS_210",
			courseName: "Getting Started with JavaScript",
			price: 550,
			discountPrice: 300,
			duration: "3h 34m",
			level: "Beginner",
			coverImage: "images/courses/course-javascript.jpg",
			teacherName: "Ted Hawkins",
			teacherPhoto: "images/teacher/ted_hawkins.jpg",
			isPopular: true,
			isTrending: true
		},
		{
			id: 8,
			courseCode: "FE_WEB_CSS_111",
			courseName: "CSS: ultimate CSS course from beginner to advanced",
			price: 750,
			discountPrice: 600,
			duration: "3h 56m",
			level: "Beginner",
			coverImage: "images/courses/course-css.jpg",
			teacherName: "Juanita Bell",
			teacherPhoto: "images/teacher/juanita_bell.jpg",
			isPopular: true,
			isTrending: true
		},
		{
			id: 14,
			courseCode: "FE_WEB_WORDPRESS_111",
			courseName: "Complete Wordpress themes & plugins",
			price: 1050,
			discountPrice: 900,
			duration: "4h 30m",
			level: "Advanced",
			coverImage: "images/courses/course-wordpress.jpg",
			teacherName: "Clevaio Simon",
			teacherPhoto: "images/teacher/clevaio_simon.jpg",
			isPopular: true,
			isTrending: false
		}
	]
}

// Tạo biến chứa array các courses
var gCourses = gCoursesDB.courses;

var gMostPopularCourses = []; // định nghĩa biến chứa các khoá học phổ biến
var gTrendingCourses = []; //định nghĩa biến chứa các khoá học xu hướng hiện nay

$(document).ready(function(){
	createMostPopularCoursesObject(gMostPopularCourses);
	loadCoursesToSection(gMostPopularCourses, 'popular');

	createTrendingCoursesObject(gTrendingCourses);
	loadCoursesToSection(gTrendingCourses, 'trending');
});
function createMostPopularCoursesObject(paramMostPopularCourses) { // Hàm đổ các khoá học phổ biến vào 
	for(var bI = 0; bI < gCourses.length; bI++) {
		if(gCourses[bI].isPopular) {
			paramMostPopularCourses.push(gCourses[bI]);
		}
	}
}

function createTrendingCoursesObject(paramTrendingCourses) { // Hàm đổ các khoá học xu hướng vào
	for(var bI = 0; bI < gCourses.length; bI++) {
		if(gCourses[bI].isTrending) {
			paramTrendingCourses.push(gCourses[bI]);
		}
	}
}

function loadCoursesToSection(paramCourses, paramSectionName) {
	for(var bI = 0; bI < paramCourses.length; bI++) {
		var vCard = $("."+paramSectionName);
		vCard.append('<div class="col-md-3 d-flex align-items-stretch">'+
									'<div class="card">'+
									'<img src="'+paramCourses[bI].coverImage+'" alt=""/>'+
									'<div class="card-body">'+
		              '<h6 class="card-title">'+paramCourses[bI].courseName+'</h6>'+
									'<p class="text"><i class="far fa-clock"></i></i>'+paramCourses[bI].duration+ " " +paramCourses[bI].level+'</p>'+
									'<span class="text"><i class="fas fa-dollar-sign"></i>'+paramCourses[bI].discountPrice+'</span>'+ " " +
									'<span class="price-discount"><i class="fas fa-dollar-sign price-discount"></i>'+paramCourses[bI].price+'</span>'+
									'</div>'+
									'<div class="card-footer text-muted">'+
									'<img class="teacher" src="'+paramCourses[bI].teacherPhoto+'"alt="">'+ " " +
									'<span class="teacher-name">'+paramCourses[bI].teacherName+'</span>'+
                  '<i class="far fa-bookmark bookmark-font"></i>'+
									'</div>'+
									'</div>'+
									'</div>'
								);
	}
	// <img src="images/courses/course-graphql.jpg" alt="">
	// <div class="card-body">
	// 	<h6 class="card-title">GraphQL: introduction to graphQL for beginners</h6>
	// 	<p class="text"><i class="far fa-clock"></i></i>2h 15m Intermediate</p>
	// 	<span class="text"><i class="fas fa-dollar-sign"></i>650</span>
	// 	<span class="price-discount"><i class="fas fa-dollar-sign price-discount"></i>850</span>
	// </div>
	// <div class="card-footer text-muted">
	// 	<img class="teacher" src="images/teacher/ted_hawkins.jpg" alt="">
	// 	<span class="teacher-name"> Ted Hawkins</span>
	// 	<i class="far fa-bookmark bookmark-font"></i>
	// </div>
}