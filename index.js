const express = require('express');
const app = express();
const port = 2242;
const path = require('path');

app.use(express.static(`views/Pizza_365`)); // Use this for show image
app.use(express.static(`views/Course_365`)); // Use this for show image

app.get("/pizza-365", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/Pizza_365/43.80.html`));
})

app.get("/course-365", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/Course_365/course_index.html`));
})

app.listen(port, () => {
  console.log(`6.90 app listening on port ${port}`);
})
